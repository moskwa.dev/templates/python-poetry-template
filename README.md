# Poetry Template

Run in poetry shell

```bash
poetry shell
```

Install the required dependencies and generate the lock

```bash
poetry install
```

You can add new dependencies with

```bash
poetry add <name-of-the-package>
```
