FROM python:3.11-buster as builder

RUN pip install poetry==1.4.2

ENV POETRY_NO_INTERACTION=1 \
    POETRY_VIRTUALENVS_IN_PROJECT=1 \
    POETRY_VIRTUALENVS_CREATE=1 \
    POETRY_CACHE_DIR=/tmp/poetry_cache

WORKDIR /src

COPY pyproject.toml poetry.lock ./
RUN touch README.md

RUN --mount=type=cache,target=$POETRY_CACHE_DIR poetry install --without dev --no-root

FROM python:3.11-slim-buster as runtime

ENV VIRTUAL_ENV=/src/.venv \
    PATH="/src/.venv/bin:$PATH"

# ARG RUN_ENV
# ENV RUN_ENV $RUN_ENV
# COPY ./envs/env.$RUN_ENV .env

COPY --from=builder ${VIRTUAL_ENV} ${VIRTUAL_ENV}

COPY poetry_template ./poetry_template

ENTRYPOINT ["python", "-m", "poetry_template.main"]
